using System;
using System.Linq;
using AutoFixture;
using Xunit;

namespace Core.Test
{
    public class PagedModelTest
    {


        [Fact]
        [Trait("Core" , "PagedModel")]
        public void When_data_is_null_or_empty_should_throw_argument_null_exception()
        {
            var pagedList  = Record.Exception(() =>  PagedModel<string>.CreatePagedList(null));
            Assert.IsType<ArgumentNullException>(pagedList);
        }



        [Theory]
        [Trait("Core", "PagedModel")]
        [InlineData(1)]
        [InlineData(2)]
        public void When_more_than_default_recordSize_present_should_paged_the_list(int pageNumber )
        {
            //ARRANGE 
            var twelveTestRecords = new Fixture().CreateMany<string>(12);
            //ACT 
            var pagedList = PagedModel<string>.CreatePagedList(twelveTestRecords.AsQueryable() , pageNumber);
            
            //ASSERT 
            switch (pageNumber)
            {
                case 1:
                    Assert.NotEmpty(pagedList.Data);
                    Assert.Equal(10 , pagedList.Data.Length);
                    Assert.Equal(2 , pagedList.TotalPages);
                    Assert.Null(pagedList.PreviousPage);
                    Assert.NotNull(pagedList.NextPage);
                    break;
                case 2 :
                    Assert.NotEmpty(pagedList.Data);
                    Assert.Equal(2 , pagedList.Data.Length);
                    Assert.Equal(2 , pagedList.TotalPages);
                    Assert.Null(pagedList.NextPage);
                    Assert.NotNull(pagedList.PreviousPage);
                    break;
            }
            
            Assert.Equal(pageNumber , pagedList.CurrentPage);
            
        }

        [Fact]
        [Trait("Core ", "PagedModel")]
        public void When_data_is_large_should_do_paging_appropriately()
        {
            //ARRANGE 
            var twelveTestRecords = new Fixture().CreateMany<string>(100);
            
            //ACT 
            var pagedList = PagedModel<string>.CreatePagedList(twelveTestRecords.AsQueryable() , pageSize:20);
            
            //ASSERT 
            Assert.NotEmpty(pagedList.Data);
            Assert.Equal(20 , pagedList.Data.Length);
            Assert.Equal(5 , pagedList.TotalPages);
            Assert.Null(pagedList.PreviousPage);
            Assert.NotNull(pagedList.NextPage);
            Assert.Equal("2" , pagedList.NextPage);

        }
        
        
    }
    
    
}