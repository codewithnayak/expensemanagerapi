namespace Core
{
    public class Result<T> 
    {
        public bool IsSuccess { get; }

        public T Payload { get; }

        public Error Error { get; }


        private Result(bool success, T payload, Error error)
        {
            IsSuccess = success;
            Payload = payload;
            Error = error;
        }

        public static Result<T> Success(T payload)
        {
            return new Result<T>(true, payload, default);
        }

        public static Result<T> Failure(Error err)
        {
            return new Result<T>(false, default, err);
        }

        public static Result<T> Failure(int errorCode, string errorMessage, string[] errors = null)
        {
            var error = new Error
            {
                ErrorCode = errorCode,
                ErrorMessage = errorMessage,
                Errors = errors
            };

            return new Result<T>(false, default, error);
        }

        public override string ToString()
        {
            return IsSuccess
                ? "Result is successful . Access payload property "
                : "Unsuccessful , access error property ";
        }
    }

    public class Error
    {
        public string ErrorMessage { get; set; }
        public int ErrorCode { get; set; }
        public string[] Errors { get; set; }
    }
}