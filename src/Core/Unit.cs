using System.Threading.Tasks;

namespace Core
{
    /// <summary>
    /// Represents a void type, since <see cref="System.Void"/> is not a valid return type in C#.
    /// </summary>
    public struct Unit
    {
        /// <summary>
        /// Default and only value of the <see cref="Unit"/> type.
        /// </summary>
        public static readonly Unit Value = new Unit();

        /// <summary>
        /// Task from a <see cref="Unit"/> type.
        /// </summary>
        public static readonly Task<Unit> Task = System.Threading.Tasks.Task.FromResult(Value);
        
        public override string ToString()
        {
            return "()";
        }
    }
}