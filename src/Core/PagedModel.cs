namespace Core
{
    using System;
    using System.Linq;

    public sealed  class PagedModel<T> where T : class 

    {
        public int TotalPages { get; }

        public int CurrentPage{ get; }

        public T[] Data { get; }

        public string NextPage { get; }

        public string PreviousPage { get; }

        private PagedModel(int totalPages, int currentPage, T[] data, string nextPage, string previousPage)
        {
            TotalPages = totalPages;
            CurrentPage = currentPage;
            Data = data;
            NextPage = nextPage;
            PreviousPage = previousPage;
        }
        
        public static  PagedModel<T> CreatePagedList(IQueryable<T> data , int pageNumber = 1 , int pageSize = 10)
        {
            if (data == null )
            {
                throw new ArgumentNullException(nameof(data));
                
            }

            var totalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(data.Count()) / Convert.ToDouble( pageSize)));
            var currentPage = pageNumber;
            var previousPage = currentPage - 1 > 0 ? Convert.ToString(currentPage -1 ) : null;
            var nextPage = totalPages - currentPage >= 1 ? Convert.ToString(currentPage + 1 ) : null;
            
            var pagedData =  data.
                            Skip(pageSize * (pageNumber - 1)).
                            Take(pageSize).
                            ToArray();
            
            return new PagedModel<T>(totalPages , currentPage , pagedData,nextPage,previousPage);
        }
        
        
        
    }
}