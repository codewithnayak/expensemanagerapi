using System;
using ExpenseManagerApi.Models;
using FluentValidation;

namespace ExpenseManagerApi.Validators
{
    public class ExpenseValidator : AbstractValidator<Expense>
    {
        public ExpenseValidator()
        {
            RuleFor(_ => _.Amount).
                GreaterThan(decimal.Zero).
                WithMessage("Amount must be greater than zero ");

            RuleFor(_ => _.PurchasedAt).
                NotEqual(DateTime.MinValue)
                .WithMessage("Purchase date should be a valid date  ");

            RuleFor(_ => _.Description).
                NotEmpty()
                .WithMessage("Expenses should have description  ");

            RuleFor(_ => _.SystemId).
                NotEmpty().
                WithMessage("Should have a system id ");

            RuleFor(_ => _.CreatedBy).
                NotEmpty().
                WithMessage("CreatedBy is a required field  ");

            RuleFor(_ => _.Tag)
                .NotEmpty()
                .WithMessage("Tag is a mandatory field ");
        }
    }
}