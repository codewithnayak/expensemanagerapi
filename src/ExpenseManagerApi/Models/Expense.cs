using System;
namespace ExpenseManagerApi.Models
{
    public class Expense
    {
        public string Name { get; set; }
        
        public string ExpenseId { get; set; }
        public DateTime PurchasedAt { get; set; }

        public DateTime CreatedAt { get; set; }
        public decimal Amount { get; set; }

        public int CurrencyId { get; set; }
        public int SystemId { get; set; }
        
        public int CategoryId { get; set; }

        public int SubCategoryId { get; set; }
        public string CreatedBy { get; set; }

        public string Description { get; set; }

        public int PriorityId { get; set; }

        public string Tag { get; set; }
    }
}