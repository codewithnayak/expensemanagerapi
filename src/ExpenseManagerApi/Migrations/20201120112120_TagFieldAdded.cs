﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ExpenseManagerApi.Migrations
{
    public partial class TagFieldAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Tag",
                table: "Expenses",
                maxLength: 100,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Tag",
                table: "Expenses");
        }
    }
}
