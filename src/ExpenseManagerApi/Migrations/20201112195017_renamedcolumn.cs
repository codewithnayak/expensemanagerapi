﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ExpenseManagerApi.Migrations
{
    public partial class renamedcolumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PurchaasedAt",
                table: "Expenses");

            migrationBuilder.AddColumn<DateTime>(
                name: "PurchasedAt",
                table: "Expenses",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PurchasedAt",
                table: "Expenses");

            migrationBuilder.AddColumn<DateTime>(
                name: "PurchaasedAt",
                table: "Expenses",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
