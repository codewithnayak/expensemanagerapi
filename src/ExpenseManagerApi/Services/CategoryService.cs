using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ExpenseManagerApi.Db;
using ExpenseManagerApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ExpenseManagerApi.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ExpenseContext _dbContext;
        private readonly IMapper _mapper;
        public CategoryService(ExpenseContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }
        
        public async Task<IEnumerable<Category>> GetCategories()
        {
            var categories = await _dbContext.Categories.AsNoTracking().ToListAsync();
            return _mapper.Map<IEnumerable<Category>>(categories);
        }
    }
}