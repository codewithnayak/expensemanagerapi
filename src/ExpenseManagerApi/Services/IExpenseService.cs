using ExpenseManagerApi.Models;

namespace ExpenseManagerApi.Services
{
    using System.Threading.Tasks;
    using Core;
    using Db.Entities;

    public interface IExpenseService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expense"></param>
        /// <returns></returns>
        Task<Result<Unit>> CreateAsync(Expense expense);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<PagedModel<ExpenseEntity>> GetExpensesAsync(int pageNumber, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteAsync(string id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="expense"></param>
        /// <returns></returns>
        Task<Result<Expense>> UpdateAsync(string id, Expense expense);
    }
}