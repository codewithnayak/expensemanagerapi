﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Core;
using ExpenseManagerApi.Db;
using ExpenseManagerApi.Db.Entities;
using ExpenseManagerApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace ExpenseManagerApi.Services
{
    public class ExpenseService : IExpenseService
    {
        private readonly ExpenseContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ILogger<ExpenseService> _logger;

        private readonly KeyValuePair<int, string> _entityNotFoundExp =
            new KeyValuePair<int, string>(101, "Entity not found found in db ");

        private readonly KeyValuePair<int, string> _expenseIdRequiredExp =
            new KeyValuePair<int, string>(102, "Expense id is a  required field ");

        public ExpenseService(ExpenseContext dbContext, IMapper mapper, ILogger<ExpenseService> logger)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _logger = logger;
        }


        public async Task<Result<Unit>> CreateAsync(Expense expense)
        {
            if (expense == null)
            {
                return Result<Unit>.Failure(StatusCodes.Status500InternalServerError, "Need an entity to add ");
            }

            try
            {
                var dataEntity = _mapper.Map<ExpenseEntity>(expense);
                dataEntity.ExpenseId = Guid.NewGuid().ToString();
                dataEntity.CreatedAt = DateTime.UtcNow;
                await _dbContext.Expenses.AddAsync(dataEntity);
                await _dbContext.SaveChangesAsync();
                return Result<Unit>.Success(Unit.Value);
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex,
                    "Db update  error : Database operation failed while creating expense for trace id ");
                return Result<Unit>.Failure(StatusCodes.Status500InternalServerError, "Something went wrong ");
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Generic error: Exception occurred while creating expense for trace id ");
                return Result<Unit>.Failure(StatusCodes.Status500InternalServerError, "Something went horribly wrong ");
            }
        }

        public Task<PagedModel<ExpenseEntity>> GetExpensesAsync(int pageNumber, int pageSize)
        {
            var expenses = _dbContext.Expenses
                .OrderByDescending(_ => _.CreatedAt)
                .AsQueryable();
            
            var pagedData = PagedModel<ExpenseEntity>.CreatePagedList(expenses, pageNumber, pageSize);
            return Task.FromResult(pagedData);
        }

        public async Task DeleteAsync(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) throw new ArgumentNullException(nameof(id));

            var expense = await TryGetExpenseEntity(id);

            if (expense == null) throw new InvalidOperationException($"No record found for the id {id} specified");

            _dbContext.Expenses.Remove(expense);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<Result<Expense>> UpdateAsync(string id, Expense expense)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return Result<Expense>.Failure(_expenseIdRequiredExp.Key, _expenseIdRequiredExp.Value);
            }

            var dataEntity = await _dbContext.Expenses
                .FirstOrDefaultAsync(_ => _.ExpenseId == id);

            if (dataEntity == null) return Result<Expense>.Failure(_entityNotFoundExp.Key, _entityNotFoundExp.Value);

            _mapper.Map(expense, dataEntity);

            _dbContext.Expenses.Update(dataEntity);

            await _dbContext.SaveChangesAsync();

            return Result<Expense>.Success(expense);
        }


        private async Task<ExpenseEntity> TryGetExpenseEntity(string expenseId)
        {
            var dataEntity = await _dbContext.Expenses
                .FirstOrDefaultAsync(_ => _.ExpenseId == expenseId);

            if (dataEntity == null)
            {
                throw new InvalidOperationException($"No record found for the id {expenseId} specified");
            }

            return dataEntity;
        }
    }
}