using System.Collections.Generic;
using System.Threading.Tasks;
using ExpenseManagerApi.Models;

namespace ExpenseManagerApi.Services
{
    public  interface ICategoryService
    {
        public Task<IEnumerable<Category>> GetCategories();
    }
}