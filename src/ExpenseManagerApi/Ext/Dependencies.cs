using ExpenseManagerApi.Models;
using ExpenseManagerApi.Services;
using ExpenseManagerApi.Validators;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;

namespace ExpenseManagerApi.Ext
{
    public static class Dependencies
    {
        public static void AddExpenseDependencies(this IServiceCollection services)
        {
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IExpenseService, ExpenseService>();
            services.AddTransient<IValidator<Expense>, ExpenseValidator>();
        }
    }
}