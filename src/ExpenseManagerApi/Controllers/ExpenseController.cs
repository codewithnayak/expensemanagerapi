using System;
using System.Threading.Tasks;
using Core;
using ExpenseManagerApi.Models;
using ExpenseManagerApi.Services;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ExpenseManagerApi.Controllers
{
    [Route("api/expense")]
    [ApiController]
    public class ExpenseController : ControllerBase
    {
        /// <summary>
        ///     Holds the business logic to add , update
        ///     delete expenses as well as other functionality as
        ///     paging , sorting etc.
        /// </summary>
        private readonly IExpenseService _expenseService;

        /// <summary>
        ///     Handles all the validation logic for expenses .
        ///     Concrete type injected in the start up  <include file='ExpenseValidator.cs' path='[@name=""]' />.
        /// </summary>
        private readonly IValidator<Expense> _validator;

        /// <summary>
        ///     Expense controller logger
        /// </summary>
        private readonly ILogger<ExpenseController> _logger;


        public ExpenseController(IExpenseService expenseService, IValidator<Expense> validator,
            ILogger<ExpenseController> logger)
        {
            _expenseService = expenseService ?? throw new ArgumentNullException(nameof(expenseService));
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        /// <summary>
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Route("")]
        public async Task<IActionResult> GetExpenses([FromQuery] int pageNumber = 1, [FromQuery] int pageSize = 10)
        {
            var response = await _expenseService.GetExpensesAsync(pageNumber, pageSize);
            return Ok(response);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}")]
        public  IActionResult GetExpense(string id)
        {
            
            return Ok();
        }

        /// <summary>
        ///     This allows user to add an expense ,
        ///     We are using FluentValidation for model validation not
        ///     the in-built dotnet core model validation .
        /// </summary>
        /// <param name="expense"></param>
        /// <returns></returns>
        [Route("add")]
        [HttpPost]
        public async Task<IActionResult> AddExpense([FromBody] Expense expense)
        {
            if (expense == null) return BadRequest("Need an expense model to save ");

            var validationResult = await _validator.ValidateAsync(expense);

            if (!validationResult.IsValid)
            {
                _logger.LogError($"Validation failed while adding expense {HttpContext.TraceIdentifier}");
                return BadRequest(Result<Unit>.Failure(400, "Validation error(s)" , validationResult.ToString(",").Split(",")));
            }

            var response = await _expenseService.CreateAsync(expense);

            return response.IsSuccess
                ? CreatedAtAction(nameof(GetExpense) ,new { id = "id"}, "success" )
                : StatusCode(StatusCodes.Status500InternalServerError, response.Error);
        }

        /// <summary>
        /// </summary>
        /// <param name="id"></param>
        /// <param name="expense"></param>
        /// <returns></returns>
        [Route("update/{id}")]
        [HttpPost]
        public async Task<IActionResult> UpdateExpense(string id, [FromBody] Expense expense)
        {
            if (!ModelState.IsValid) return BadRequest();

            var response = await _expenseService.UpdateAsync(id, expense);

            return response.IsSuccess
                ? Ok(response.Payload)
                : StatusCode(StatusCodes.Status500InternalServerError, response.Error);
        }

        /// <summary>
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteExpense(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return BadRequest("Expense id is a required field ");

            await _expenseService.DeleteAsync(id);

            return NoContent();
        }
    }
}