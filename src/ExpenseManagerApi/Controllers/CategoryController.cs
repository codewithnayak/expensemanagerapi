using System.Threading.Tasks;
using ExpenseManagerApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace ExpenseManagerApi.Controllers
{
    
    [Route("api/category")]
    public class CategoryController : ControllerBase
    {

        private readonly ICategoryService _categoryService;
        
        public CategoryController( ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }


        [Route("")]
        public async Task<IActionResult> Get()
        {
            var categories = await _categoryService.GetCategories();
            return Ok(categories);
        }
    }

  
}