using System.Collections.Generic;
using ExpenseManagerApi.Db.Entities;
using Microsoft.EntityFrameworkCore;

namespace ExpenseManagerApi.Db
{
    public class ExpenseContext : DbContext
    {

        public DbSet<ExpenseEntity> Expenses { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Currency> Currencies { get; set; }
        public ExpenseContext(DbContextOptions<ExpenseContext> options) : base(options)
        {

            this.Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(new List<Category>
            {
                new Category
                {
                    Id=1,
                    CategoryName = "Grocery"
                },
                new Category
                {
                    Id=2,
                    CategoryName = "Petrol"
                },
                new Category
                {
                    Id=3,
                    CategoryName = "Mobile-Recharge"
                }

            });
            modelBuilder.Entity<Currency>().HasData(new List<Currency>
            {
                new Currency
                {
                    Id=1,
                    CountryName = "GreatBritain",
                    CurrencyCode = "GBP",
                    CurrencySymbol = "£"
                },
                new Currency
                {
                    Id=2,
                    CountryName = "USA",
                    CurrencyCode = "USD",
                    CurrencySymbol = "$"
                },
                new Currency
                {
                    Id=3,
                    CountryName = "India",
                    CurrencyCode = "INR",
                    CurrencySymbol = "₹"
                }
                

            });
            base.OnModelCreating(modelBuilder);
        }
    }
}