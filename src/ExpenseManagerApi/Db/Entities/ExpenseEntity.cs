using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

namespace ExpenseManagerApi.Db.Entities
{
    public class ExpenseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string ExpenseId { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [Required]
        public DateTime PurchasedAt { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
        [Required]
        [Column(TypeName = "decimal(5, 2)")]
        public decimal Amount { get; set; }
        public int SystemId { get; set; }
        public int SubCategoryId { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        public int PriorityId { get; set; }
        [Required]
        [MaxLength(100)]
        public string Tag { get; set; }
        //Navigational properties 
        [ForeignKey("Currency")]
        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }
        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}