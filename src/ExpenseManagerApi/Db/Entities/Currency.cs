using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExpenseManagerApi.Db.Entities
{
    public class Currency
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(10)]
        public string CurrencyCode { get; set; }
        [MaxLength(10)]
        public string CurrencySymbol { get; set; }
        [MaxLength(50)]
        public string CountryName { get; set; }
    }
}