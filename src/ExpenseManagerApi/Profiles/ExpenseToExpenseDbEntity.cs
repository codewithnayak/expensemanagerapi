using AutoMapper;
using ExpenseManagerApi.Db.Entities;
using ExpenseManagerApi.Models;
using ExpenseManagerApi.Services;
using Category = ExpenseManagerApi.Models.Category;

namespace ExpenseManagerApi.Profiles
{
    public class ExpenseToExpenseDbEntity : Profile
    {
        public ExpenseToExpenseDbEntity()
        {
            CreateMap<Expense, ExpenseEntity>()
            .ForMember(dest => dest.Id, opt => opt.Ignore())
            .ReverseMap();
        }

    }
    
    public class CategoryToCategoryDbEntity : Profile
    {
        public CategoryToCategoryDbEntity()
        {
            CreateMap<Category, ExpenseManagerApi.Db.Entities.Category>()
                .ReverseMap();
        }

    }
    
    
}